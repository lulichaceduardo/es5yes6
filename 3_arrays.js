
var yearsOfBirth = [1990, 2001, 1985, 1950, 2010];

// Es5
var ages = [];
for(var i = 0; i< yearsOfBirth.length; i++){
  ages.push(2019-yearsOfBirth[i]);
}
// console.log(yearsOfBirth);
// console.log(ages);

// Es6 map, find
const calculateAge = (item)=> 2019 - item;
const agesEs6 = yearsOfBirth.map(calculateAge);

// console.log(yearsOfBirth);
// console.log(agesEs6);

const persons = [
  {name: 'eduardo', age: 29},
  {name: 'randy', age: 23},
  {name: 'ariadna', age: 23},
  {name: 'paul', age: 25}
]

const encontrarEdad23 = persons.find((item)=>{
  return item.age === 23
})

const filtrar23 = persons.filter((item)=> item.age === 23)
// console.log(filtrar23);

const mayorEdad = persons.every((item)=> item.age>18);

const mayorEdadSome = persons.some((item)=> item.age<18);
// console.log(mayorEdadSome);

const sumaEdades = persons.reduce((total, item)=>{ 
  return total+item.age;
},0);

console.log(sumaEdades/persons.length);