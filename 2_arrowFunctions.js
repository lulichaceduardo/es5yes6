/** Arrow functions o Funciones flecha */

// Es5

function saludar(){
  console.log('Bienvenidos a la clase de react');
}
// saludar();

function sumar(a,b) {
  return a+b;
}

// console.log(sumar(10, 5));

var restar = function(a, b) {
  return a-b;
}

// console.log(restar(10, 5));

// Es6

const restando = (a,b) => a-b;

const sumando = (a,b) => {
  return a + b;
}

console.log(sumando(10, 6));