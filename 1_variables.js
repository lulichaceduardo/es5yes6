//ES5 y Es6
/** Hablemos de var let y const y los template literal */

//ES5
var nombre = 'eduardo';
// nombre = 23;

// console.log(nombre);

//ES6

let apellido = 'lulichac';
const anioNacimiento = 1990;

// console.log('mi nombre es: ' + nombre + ' ' + apellido + ' naci en 1990');
// Template Literal
// console.log(`mi nombre es ${nombre} ${apellido} y naci en ${anioNacimiento}`);

const persona = {
  nombre: 'eduardo',
  apellido: 'lulichac'
}

// console.log(persona);

persona.nombre = 'Juan';
// console.log(persona);


function pruebaEs5() {
  if(true) {
    var person = 'Eduardo';
  }
  console.log(person);
}

function pruebaEs6(){
  let apellido1
  const edad = 29;
  if(true) {
    apellido1 = 'lulichac';
  }
  console.log(apellido1);
  console.log(edad);
}

pruebaEs6();
