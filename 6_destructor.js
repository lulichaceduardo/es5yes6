//Destructing (Destructuración o desacoplamiento de objetos)
//Destructing (Destructuración o desacoplamiento de objetos)

var arrayDeNumeros = [1,2,3];

// const [a,,,] = arrayDeNumeros;

var listaDePersonas = [
  {name: 'eduardo', age: 29},
  {name: 'ariadna', age: 22},
  {name: 'paul', age: 23},
  {name: 'randy', age: 24},
]

const [persona1] = listaDePersonas;

const {age} = persona1;

console.log(age);