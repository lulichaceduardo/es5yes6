// SPREAD y REST

// Array y Objetos

const numeros = [1,2,3,4,5];
const otrosNumeros = [...numeros, 6,7,8,9];

// console.log(...numeros);
// console.log(otrosNumeros);

const jobInformation = {
  job: 'teacher',
  where: 'Academias moviles'
};

const user = {
  name: 'eduardo',
  age: 29,
  ...jobInformation
}

// console.log(jobInformation);
// console.log(user);


const sumaItems = (...arguments)=>{
  console.log(arguments);
  return arguments.reduce((total, item)=>{
    return total+item;
  }, 0)
}

console.log(sumaItems(1,2,3,4,5,6,7,8));