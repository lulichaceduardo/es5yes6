// Es6 

class Person {
  constructor(gender, name){
    this.gender = gender;
    this.name = name;
  }
  consultaGenero = () => {
    console.log(`Hola me llamo ${this.name} y mi genero es, ${this.gender}`);
  }
}

class Doctor extends Person{
  constructor(name, especialidad, gender){
    super(gender, name);
    this.epsecialidad = especialidad;
  }
}

const cirujano = new Doctor('Eduardo', 'Cirugia', 'masculino');

cirujano.consultaGenero();
// console.log(cirujano);
// const persona1 = new Person('masculino', 'eduardo');

// console.log(persona1);

// persona1.consultaGenero();

var Persona = function(name, gender) {
  this.name = name;
  this.gender = gender;
}

var personita1 = new Persona('Maria', 'femenino');

Persona.prototype.consultaGenero = function(){
  console.log(`Hola me llamo ${this.name} y mi genero es, ${this.gender}`);
}

personita1.consultaGenero();
// console.log(personita1);